﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(string email, string pwd)
        {
            Entities db = new Entities();
            var user = db.User.Where(d => d.Email == email && d.Password == pwd).FirstOrDefault();
            if (user != null)
            {
                return RedirectToAction("Index");

            }
            else
            {
                ViewBag.Message = "Error email";
            }


            return View();
        }
	}
}