﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
    }

    public class UserMetadata
    {
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("姓名")]
        [StringLength(5)]
        public string Name { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("Email")]
        [EmailAddress]
        public string Email { get; set; }

        public string Password { get; set; }
        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("生日")]
        public System.DateTime Birthday { get; set; }

        [Required(ErrorMessage = "必填欄位")]
        [DisplayName("性別")]
        public int sex { get; set; }
        //將User.cs的程式碼複製到這邊
    } 

}